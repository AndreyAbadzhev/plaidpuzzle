//
//  AppDelegate.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit
import GoogleMobileAds
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, DataStorageProtocol {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        if !_dataStorageMusicMuted { DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { Music.shared().pause() }) }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if !_dataStorageMusicMuted { DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: { Music.shared().play() }) }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

