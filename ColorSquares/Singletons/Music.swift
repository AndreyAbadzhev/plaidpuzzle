//
//  Music.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 30/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import AVFoundation
class Music: NSObject, AVAudioPlayerDelegate {

    private static var instance: Music?
    private var player: AVAudioPlayer?
    
    override private init () {
        super.init()
        constructPlayer()
    }
    
    static func shared() -> Music {
        if instance == nil {
            instance = Music()
        }
        return instance!
    }

    func play()  {
        player?.play()
        player?.setVolume(0.3, fadeDuration: 3)
    }
    
    func pause() {
        player?.setVolume(0, fadeDuration: 1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: { self.player?.pause() })
    }
    
    func setVolume(value: Float, duration: TimeInterval) {
        player?.setVolume(value, fadeDuration: duration)
    }
    
    private func constructPlayer() {
        guard let url = Bundle.main.url(forResource: "mainTheme", withExtension: "mp3") else { return }
        do {
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            player?.delegate = self
            player?.volume = 0
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag { player.volume = 0; play() }
    }
}
