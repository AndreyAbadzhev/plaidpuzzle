//
//  DataStorage.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation

protocol DataStorageProtocol {}

extension DataStorageProtocol {
    
    var _dataStorageIfFirstGame: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "firstGame")
        }
    }
    
    var _dataStorageLevel: Int {
        get {
            if UserDefaults.standard.integer(forKey: "level") != 0 {
                return UserDefaults.standard.integer(forKey: "level")
            } else {
                _dataStorageIncrementLevel()
                return 1
            }
        }
    }
    
    var _dataStorageGamesPlayed: Int {
        get {
            return UserDefaults.standard.integer(forKey: "gamesPlayed")
        }
    }
    
    var _dataStorageHardScore: Int {
        get {
            return UserDefaults.standard.integer(forKey: "hardScore")
        }
    }
    
    var _dataStorageHellScore: Int {
        get {
            return UserDefaults.standard.integer(forKey: "hellScore")
        }
    }
    
    var _dataStorageSoundsMuted: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "soundsMuted")
        }
    }
    
    var _dataStorageMusicMuted: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "musicMuted")
        }
    }
    
    var _dataStorageHardFirstGamePlayed: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "hardFirstGamePlayed")
        }
    }
    
    var _dataStorageHellFirstGamePlayed: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "hellFirstGamePlayed")
        }
    }
    
    func _dataStorageTryMakeInitialRecord() {
        if UserDefaults.standard.bool(forKey: "firstLaunchHadPlace") == true {
            return
        } else {
            UserDefaults.standard.set(true, forKey: "firstLaunchHadPlace")
            UserDefaults.standard.set(true, forKey: "firstGame")
            
            UserDefaults.standard.set(1, forKey: "level")
            UserDefaults.standard.set(0, forKey: "gamesPlayed")
            
            UserDefaults.standard.set(1000, forKey: "hardScore")
            UserDefaults.standard.set(1000, forKey: "hellScore")
            
            UserDefaults.standard.set(false, forKey: "soundsMuted")
            UserDefaults.standard.set(false, forKey: "musicMuted")
            
            UserDefaults.standard.set(false, forKey: "hardFirstGamePlayed")
            UserDefaults.standard.set(false, forKey: "hellFirstGamePlayed")
        }
    }
    
    func _dataStorageFirstGameHadPlace() {
        UserDefaults.standard.set(false, forKey: "firstGame")
    }
    
    func _dataStorageIncrementLevel() {
        UserDefaults.standard.set(_dataStorageLevel + 1, forKey: "level")
    }
    
    func _dataStorageIncrementGamesPlayed() {
        UserDefaults.standard.set(_dataStorageGamesPlayed + 1, forKey: "gamesPlayed")
    }
    
    func _dataStorageUpdateHardScore(value: Int) {
        UserDefaults.standard.set(_dataStorageHardScore + value, forKey: "hardScore")
    }
    
    func _dataStorageUpdateHellScore(value: Int) {
        UserDefaults.standard.set(_dataStorageHellScore + value, forKey: "hellScore")
    }
    
    func _dataStorageSoundsMutedChangeStatus() {
        UserDefaults.standard.set(!_dataStorageSoundsMuted, forKey: "soundsMuted")
    }
    
    func _dataStorageMusicMutedChangeStatus() {
        UserDefaults.standard.set(!_dataStorageMusicMuted, forKey: "musicMuted")
    }
    
    func _dataStorageHardFirstGamePlayedChangeStatus() {
        UserDefaults.standard.set(true, forKey: "hardFirstGamePlayed")
    }
    
    func _dataStorageHellFirstGamePlayedChangeStatus() {
        UserDefaults.standard.set(true, forKey: "hellFirstGamePlayed")
    }
    
    
    //Debug
    
    func _dataStorageDebugClearProgress() {
        UserDefaults.standard.set(1, forKey: "level")
        UserDefaults.standard.set(1000, forKey: "hardScore")
        UserDefaults.standard.set(1000, forKey: "hellScore")
        UserDefaults.standard.set(0, forKey: "gamesPlayed")
    }
    
}
