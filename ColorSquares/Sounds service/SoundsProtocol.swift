//
//  SoundsProtocol.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 26/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import AVFoundation

enum SoundType {
    case tap
    case cellSelect
    case cellUnselect
    case cellRecolorError
    case victory
    case defeat
}

protocol SoundsProtocol: DataStorageProtocol {}

extension SoundsProtocol {
    
    func _soundsPlaySound(soundType: SoundType) {
        if _dataStorageSoundsMuted { return }
        
        var soundName: String = ""
        
        switch soundType {
        case .tap: soundName = "NewClick"//"Click"
        case .cellSelect: soundName = "CellSelect"
        case .cellUnselect: soundName = "RecolorError"
        case .cellRecolorError: soundName = "RecolorError"; AudioServicesPlayAlertSound(SystemSoundID(4095))
        case .victory: soundName = "Victory"
        case .defeat: AudioServicesPlayAlertSound(SystemSoundID(4095))
        }
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "mp3") else { return }

        do {
            var player: AVAudioPlayer? = try AVAudioPlayer(contentsOf: url)
            
            switch soundType {
            case .cellSelect, .cellUnselect, .cellRecolorError, .defeat: player?.volume = 1
            case .victory, .tap: player?.volume = 5
            }

            player?.play()
            
            if soundType == .victory && !_dataStorageMusicMuted { Music.shared().setVolume(value: 0.05, duration: 0) }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + getSoundDuration(url: url), execute: {
                player?.stop()
                player = nil
                if soundType == .victory && !self._dataStorageMusicMuted { Music.shared().setVolume(value: 0.3, duration: 3) }
            })

        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    private func getSoundDuration(url: URL) -> Float64 {
        let asset = AVURLAsset(url: url, options: nil)
        let audioDuration = asset.duration
        return CMTimeGetSeconds(audioDuration)
    }
}
