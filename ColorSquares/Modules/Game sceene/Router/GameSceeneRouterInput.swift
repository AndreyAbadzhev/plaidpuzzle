//
//  GameSceeneRouterInput.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
protocol GameSceeneRouterInput {
    func showAlertAboutRecolorError()
    func showAlertAboutVictory(difficulty: Difficulty?, nextLevel: @escaping() -> ())
    func showAlertAboutDefeat(playAgain: @escaping() -> ())
    func showRules()
    func backToMenu()
}
