//
//  GameSceeneRouter.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileAds
class GameSceeneRouter: NSObject, GameSceeneRouterInput, DataStorageProtocol {
    weak var view: GameSceeneViewController?
    
    var interstitial: GADInterstitial!
    var interstitialClosed: (() -> ())?
    
    required init(view: GameSceeneViewController) {
        self.view = view
    }
    
    func showAlertAboutRecolorError() {
        let alertView = Bundle.main.loadNibNamed(String(describing: AlertView.self), owner: nil, options: nil)?[0] as? AlertView
        alertView?.titleLabel.text = "RULES"
        alertView?.messageLabel.text = "You may only recoler neibour cells!"
        alertView?.leftButton.setTitle("I got it", for: .normal)
        alertView?.leftButton.setTitleColor(UIColor.tintGreen, for: .normal)
        alertView?.leftButtonAction = { [weak alertView] in alertView?.hide { alertView?.removeFromSuperview() } }
        alertView?.rightButton.isHidden = true
        alertView?.frame = view?.view.frame ?? CGRect()
        view?.view.addSubview(alertView ?? AlertView())
        alertView?.show()
    }
    
    func showAlertAboutVictory(difficulty: Difficulty?, nextLevel: @escaping() -> ()) {
        guard let difficulty = difficulty else { self.backToMenu(); return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            let alertView = Bundle.main.loadNibNamed(String(describing: AlertView.self), owner: nil, options: nil)?[0] as? AlertView
            alertView?.titleLabel.text = "WELL DONE!"
            
            if difficulty == .levelDifficulty { alertView?.messageLabel.text = "Level \(self._dataStorageLevel - 1) completed!" }
            if difficulty == .hardDifficulty { alertView?.messageLabel.text = "You have completed HARD mode! You recieve 550 points minus those, you lost during the game! Hope, you are in plus!" }
            if difficulty == .hellDifficulty { alertView?.messageLabel.text = "You have completed HELL mode! You recieve 1200 points minus those, you lost during the game! Hope, you are in plus!" }
            
            alertView?.leftButton.setTitle("Back to menu", for: .normal)
            alertView?.leftButton.setTitleColor(UIColor.textColorNormal, for: .normal)
            alertView?.leftButtonAction = { [weak alertView] in alertView?.hide { alertView?.removeFromSuperview(); self.backToMenu() } }
            
            if difficulty == .levelDifficulty { alertView?.rightButton.setTitle("Play next", for: .normal) }
            if difficulty != .levelDifficulty { alertView?.rightButton.setTitle("Play again", for: .normal) }
            
            alertView?.rightButton.setTitleColor(UIColor.tintGreen, for: .normal)
            alertView?.rightButtonAction = { [weak alertView] in alertView?.hide { alertView?.removeFromSuperview(); self.interstitialClosed = { nextLevel() }; self.showInterstitial(completion: self.interstitialClosed!) } }
            alertView?.frame = self.view?.view.frame ?? CGRect()
            self.view?.view.addSubview(alertView ?? AlertView())
            alertView?.show()
        })
    }

    func showRules() {
        let onboardingAlertView = Bundle.main.loadNibNamed(String(describing: OnboardingAlertView.self), owner: nil, options: nil)?[0] as? OnboardingAlertView
        onboardingAlertView?.okButtonAction = { [weak onboardingAlertView] in onboardingAlertView?.hide { onboardingAlertView?.removeFromSuperview() } }
        onboardingAlertView?.frame = view?.view.frame ?? CGRect()
        view?.view.addSubview(onboardingAlertView ?? AlertView())
        onboardingAlertView?.show()
    }
    
    
    
    func showAlertAboutDefeat(playAgain: @escaping() -> ()) {
        let alertView = Bundle.main.loadNibNamed(String(describing: AlertView.self), owner: nil, options: nil)?[0] as? AlertView
        alertView?.titleLabel.text = "Oh..."
        alertView?.messageLabel.text = "It seems like you have consumed all the cells of a color, and now will not be able to win..."
        
        alertView?.leftButton.setTitle("Back to menu", for: .normal)
        alertView?.leftButton.setTitleColor(UIColor.textColorNormal, for: .normal)
        alertView?.leftButtonAction = { [weak alertView] in alertView?.hide { alertView?.removeFromSuperview(); self.backToMenu() } }
        
        alertView?.rightButton.setTitle("Play again", for: .normal)
        alertView?.rightButton.setTitleColor(UIColor.tintGreen, for: .normal)
        alertView?.rightButtonAction = { [weak alertView] in alertView?.hide { alertView?.removeFromSuperview(); self.interstitialClosed = { playAgain() }; self.showInterstitial(completion: self.interstitialClosed!) } }
        
        alertView?.frame = view?.view.frame ?? CGRect()
        view?.view.addSubview(alertView ?? AlertView())
        alertView?.show()
    }
    
    func backToMenu() {
        interstitialClosed = { [weak self] in self?.view?.dismiss(animated: false, completion: nil) }
        showInterstitial(completion: interstitialClosed!)
    }

    deinit {
        print("GS router deallocated")
    }
}

extension GameSceeneRouter: GADInterstitialDelegate {
   
    //ADS full screen banner
    
    func preareInterstitial() {
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-4725748340314469/5783662882")  /*"ca-app-pub-3940256099942544/4411468910"*/
        let request = GADRequest()
        interstitial.load(request)
        interstitial.delegate = self
    }
    
    func showInterstitial(completion: @escaping() -> ()) {
        if interstitial.isReady {
            guard let gsViewController = view else { return }
            if !_dataStorageMusicMuted { Music.shared().pause() }
            interstitial.present(fromRootViewController: gsViewController)
        } else {
            completion() //Рекламы нет (или нет интернета), либо она не успела подгрузиться. Тогда скипаю рекламный блок и сразу вызываю completion()
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        if !_dataStorageMusicMuted { Music.shared().play() }
        interstitialClosed?()
    }
    
}

