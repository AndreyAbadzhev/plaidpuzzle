//
//  GameSceeneConfigurator.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
class GameSceeneConfigurator {
    
    var difficulty: Difficulty
    
    required init(difficulty: Difficulty) {
        self.difficulty = difficulty
    }
    
    func configure(with viewController: GameSceeneViewController) {
        let presenter = GameSceenePresenter()
        let interactor = GameSceeneInteractor(difficulty: difficulty)
        let router = GameSceeneRouter(view: viewController)
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.preareInterstitial() //ADS full screen
    }
    
    deinit {
        print("GS configurator deallocated")
    }
}
