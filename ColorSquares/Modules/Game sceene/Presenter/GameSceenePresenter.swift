//
//  GameSceenePresenter.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit

class GameSceenePresenter: GameSceeneViewOutput, SoundsProtocol {
    weak var view: GameSceeneViewInput?
    var interactor: GameSceeneInteractorInput?
    var router: GameSceeneRouterInput?
    
    var numberOfSquares: Int? {
        return interactor?.numberOfSquares
    }
    
    var selectedCellIndexPath: IndexPath? {
        return interactor?.selectedCellIndexPath
    }
    
    func viewIsReady() {
        view?.setupInitialState(completion: { [weak self] in self?.interactor?.prepareForStartGame() })
    }
    
    func startGame(difficulty: Difficulty, numbersArray: [Int], mainColorsArray: [UIColor], tintColorsArray: [UIColor], colorsSumms: ColorsSumms, colorsSummsRule: ColorsSummsRule, moves: Int, time: Int, score: Int, isFirstGame: Bool, completion: @escaping() -> ()) {
        var title = ""
        var additionalFieldHidden: Bool = false
        
        switch difficulty {
        case .levelDifficulty: title = "- \(interactor?.level ?? 1) -"; additionalFieldHidden = true
        case .hardDifficulty: title = "Hard".uppercased(); additionalFieldHidden = false
        case .hellDifficulty: title = "Hell".uppercased(); additionalFieldHidden = false
        }
        
        let moves = "Moves: \(moves)".uppercased()
        let time = "Time: \(time)".uppercased()
        let score = "Score: \(score)".uppercased()

        let cellsDataArray = fillCellsDataArray(numbersArray: numbersArray, mainColorsArray: mainColorsArray, tintColorsArray: tintColorsArray)
        
        self.view?.startGame(title: title, cellsDataArray: cellsDataArray, additionalFieldHidden: additionalFieldHidden, colorsSumms: colorsSumms, colorsSummsRule: colorsSummsRule, moves: moves, time: time, score: score, completion: { [weak self] in if isFirstGame { self?.showRulesForFirstGame() }; completion() })
        
    }
    
    func fillCellsDataArray(numbersArray: [Int], mainColorsArray: [UIColor], tintColorsArray: [UIColor]) -> [CellData] {
        var arrayToReturn: [CellData] = []
        for (index, _) in numbersArray.enumerated() {
            let section = index / Int(Double(numbersArray.count).squareRoot())
            let row = index % Int(Double(numbersArray.count).squareRoot())
            let indexPath = IndexPath(row: row, section: section)
            arrayToReturn.append(CellData(indexPath: indexPath, number: numbersArray[index], mainColor: mainColorsArray[index], tintColor: tintColorsArray[index]))
        }
        return arrayToReturn
    }
    
    
    func setSelectedCellIndexPath(indexPath: IndexPath) {
        interactor?.setSelectedCellIndexPath(indexPath: indexPath, unselectChosenCell: { [weak self] in
            self?._soundsPlaySound(soundType: .cellUnselect)
            self?.setCellSelected(indexPath: indexPath, selectionType: .unselected)
            
        }, chooseCorrectCell: { [weak self] in
            self?._soundsPlaySound(soundType: .cellSelect)
            self?.setCellSelected(indexPath: indexPath, selectionType: .selected)
            
        }, choseIncorrectCell: { [weak self] in
            self?._soundsPlaySound(soundType: .cellRecolorError)
            self?.setCellSelected(indexPath: self?.interactor?.selectedCellIndexPath ?? IndexPath(), selectionType: .unselected)
            self?.router?.showAlertAboutRecolorError()
            
        }, relocorCell: { [weak self] (newMainColor, newTintColor) in
            self?._soundsPlaySound(soundType: .cellSelect)
            self?.setCellSelected(indexPath: self?.interactor?.selectedCellIndexPath ?? IndexPath(), selectionType: .unselected)
            self?.updateCell(indexPath: indexPath, number: nil, mainColor: newMainColor, tintColor: newTintColor)
            
        }, recolorCellWithTheSameColor: { [weak self] in
            self?._soundsPlaySound(soundType: .cellUnselect)
            self?.setCellSelected(indexPath: self?.interactor?.selectedCellIndexPath ?? IndexPath(), selectionType: .unselected)
            
        }, victory: { [weak self] in
            self?._soundsPlaySound(soundType: .victory)
            self?.router?.showAlertAboutVictory(difficulty: self?.interactor?.difficulty, nextLevel: {
                self?.view?.prepareViewsForReloadGame(completion: { [weak self] in self?.interactor?.prepareForStartGame() })
            })
            
        }, defeat: { [weak self] in
            self?._soundsPlaySound(soundType: .defeat)
            self?.router?.showAlertAboutDefeat(playAgain: {
                self?.view?.prepareViewsForReloadGame(completion: { [weak self] in self?.interactor?.prepareForStartGame() })
            })
        })
    }

    func backButtonDidTap() {
        _soundsPlaySound(soundType: .tap)
        interactor?.prepareForEndGame(victory: false)
        router?.backToMenu()
    }
    
    func rulesButtonDidTap() {
        _soundsPlaySound(soundType: .tap)
        router?.showRules()
    }
    
    func showRulesForFirstGame() {
        router?.showRules()
    }
    
    deinit {
        print("GS presenter deallocated")
    }
}

extension GameSceenePresenter: GameSceeneInteractorOutput {

    func updateTimerLabel(value: Int) {
        view?.updateTimerLabel(value: "Time: \(value)".uppercased())
    }
    
    func updateMovesMadeLabel(value: Int) {
        view?.updateMovesMadeLabel(value: "Moves: \(value)".uppercased())
    }
    
    func updateScoreLabel(value: Int) {
        view?.updateScoreLabel(value: "Score: \(value)".uppercased())
    }
    
    func updateSummsLabels(colorsSumms: ColorsSumms, colorsSummsRule: ColorsSummsRule) {
        view?.updateSummsLabels(colorsSumms: colorsSumms, colorsSummsRule: colorsSummsRule)
    }
    
    func updateCell(indexPath: IndexPath, number: Int?, mainColor: UIColor, tintColor: UIColor) {
        view?.updateCell(indexPath: indexPath, number: number, mainColor: mainColor, tintColor: tintColor)
    }
    func setCellSelected(indexPath: IndexPath, selectionType: Selection) {
        view?.setCellSelected(indexPath: indexPath, selectionType: selectionType)
    }
}


