//
//  GameCell.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit

class GameCell: UICollectionViewCell, SoundsProtocol {

    @IBOutlet weak var _backgroundView: UIView!
    @IBOutlet weak var numberLabel: UILabel!

    func prepareBlancCell() {
        numberLabel.text = "?"
        _backgroundView.layer.borderWidth = 1
        _backgroundView.layer.cornerRadius = 3
        
        UIView.animate(withDuration: 0.3, animations: {
            self.numberLabel.textColor = UIColor.lightGray
            self._backgroundView.layer.borderColor = UIColor.lightGray.cgColor
            self._backgroundView.backgroundColor = UIColor.white
        })
    }
    
    func fillCell(number: Int?, mainColor: UIColor, tintColor: UIColor) {
        
        if let number = number {
            numberLabel.text = String(number)
        }

        UIView.animate(withDuration: 0.4, animations: {
            self._backgroundView.layer.borderColor = tintColor.cgColor
            self._backgroundView.backgroundColor = mainColor
            
            self._backgroundView.transform = self._backgroundView.transform.rotated(by: .pi)
        })
        
        UIView.animate(withDuration: 0.2, animations: {
            self.numberLabel.transform = self.numberLabel.transform.scaledBy(x: 1, y: 0.01)
        }, completion: { _ in
            self.numberLabel.textColor = tintColor
            UIView.animate(withDuration: 0.2, animations: {
                self.numberLabel.transform = self.numberLabel.transform.scaledBy(x: 1, y: 100)
                
            })
        })
        
    }
    
    func configureSelect(selection: Selection, subselectionColor: UIColor) {

        let backgroundColor = _backgroundView.backgroundColor
        
        switch selection {
        case .selected:
            
            UIView.animate(withDuration: 0.4, animations: {
                switch backgroundColor {
                case UIColor.mainRed: self._backgroundView.backgroundColor = .tintRed
                case UIColor.mainBlue: self._backgroundView.backgroundColor = .tintBlue
                case UIColor.mainGreen: self._backgroundView.backgroundColor = .tintGreen
                case UIColor.mainYellow: self._backgroundView.backgroundColor = .tintYellow
                case UIColor.mainPurple: self._backgroundView.backgroundColor = .tintPurple
                default: break
                }
            })
            
            UIView.animate(withDuration: 0.2, animations: {
                self.numberLabel.transform = self.numberLabel.transform.scaledBy(x: 1.333333, y: 1.333333)
                self.numberLabel.textColor = UIColor.white
            })
            
        case .unselected:

            UIView.animate(withDuration: 0.4, animations: {
                switch backgroundColor {
                case UIColor.tintRed: self._backgroundView.backgroundColor = .mainRed
                case UIColor.tintBlue: self._backgroundView.backgroundColor = .mainBlue
                case UIColor.tintGreen: self._backgroundView.backgroundColor = .mainGreen
                case UIColor.tintYellow: self._backgroundView.backgroundColor = .mainYellow
                case UIColor.tintPurple: self._backgroundView.backgroundColor = .mainPurple
                default: break
                }
            })

            UIView.animate(withDuration: 0.2, animations: {
                self.numberLabel.transform = self.numberLabel.transform.scaledBy(x: 0.75, y: 0.75)
                self.numberLabel.textColor = backgroundColor
            })
        }
    }
}

enum Selection {
    case selected
    case unselected
}
