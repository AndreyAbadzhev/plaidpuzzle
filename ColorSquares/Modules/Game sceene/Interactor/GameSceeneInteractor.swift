//
//  GameSceeneInteractor.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit

struct ColorsSumms {
    var red: Int = 0
    var yellow: Int = 0
    var blue: Int = 0
    var green: Int = 0
    var purple: Int = 0
}

struct ColorsSummsRule {
    var red: Int = 0
    var yellow: Int = 0
    var blue: Int = 0
    var green: Int = 0
    var purple: Int = 0
}

class GameSceeneInteractor: GameSceeneInteractorInput, DataStorageProtocol {

    weak var output: GameSceeneInteractorOutput?
    
    var difficulty: Difficulty
    var levelDifficulty: LevelDifficulty
    
    var selectedCellIndexPath: IndexPath?
    
    var randomNumbersArray: Array<Int> = []
    var randomMainColorsArray: Array<UIColor> = []
    var randomTintColorsArray: Array<UIColor> = []
    
    var colorSumms = ColorsSumms()
    var colorsSummsRule = ColorsSummsRule()
    
    var numberOfMovesMade: Int = 0
    var gameTimer: Timer?
    var moveElaspedTimeSeconds: Int = 0
    var moveElaspedTimeSecondsArray: [Int] = []
    
    var points = 0

    var level: Int {
        return _dataStorageLevel
    }
    
    var numberOfSquares: Int? {
        return levelDifficulty.numberOfSquares
    }
    
    required init(difficulty: Difficulty) {
        self.difficulty = difficulty
        levelDifficulty = LevelDifficulty(difficulty: difficulty, level: 1)
    }
    
    func prepareForStartGame() {
        levelDifficulty = LevelDifficulty(difficulty: difficulty, level: level)
        fillRandomNumersArray()
        if fillRandomColorsArray() == false { prepareForStartGame(); return } //False если в массиве цветов не хватает какого-то одного цвета. Запускаю prepareForStart еще раз
        countSummsForColors()
        countSummsRuleForColors()
        output?.startGame(difficulty: difficulty, numbersArray: randomNumbersArray, mainColorsArray: randomMainColorsArray, tintColorsArray: randomTintColorsArray, colorsSumms: colorSumms, colorsSummsRule: colorsSummsRule, moves: 0, time: 0, score: 0, isFirstGame: _dataStorageIfFirstGame, completion: { [weak self] in self?.startTimer(); self?._dataStorageFirstGameHadPlace() })
    }
    
    func prepareForEndGame(victory: Bool) {
        stopTimer()
        numberOfMovesMade = 0
        points = 0
        moveElaspedTimeSecondsArray.removeAll()
        selectedCellIndexPath = nil
        if victory {
            switch difficulty {
            case .levelDifficulty: _dataStorageIncrementLevel()
            default: addVictoryPoints()
            }
        }
    }
    
    func addVictoryPoints() {
        switch difficulty {
        case .levelDifficulty: break
        case .hardDifficulty: _dataStorageUpdateHardScore(value: 550)
        case .hellDifficulty: _dataStorageUpdateHellScore(value: 1200)
        }
    }
    
    func startTimer() {
        if difficulty != .levelDifficulty {
            self.gameTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateElaspedTime), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer() {
        gameTimer?.invalidate()
    }
    
    @objc func updateElaspedTime() {
        moveElaspedTimeSeconds += 1
        output?.updateTimerLabel(value: moveElaspedTimeSeconds)
    }
    
    func checkForWinCondition(victory: @escaping() -> ()) {
        if colorsSummsRule.red != colorSumms.red { return }
        if colorsSummsRule.blue != colorSumms.blue { return }
        if colorsSummsRule.green != colorSumms.green { return }
        if colorsSummsRule.purple != colorSumms.purple { return }
        if colorsSummsRule.yellow != colorSumms.yellow { return }
        victory()
        prepareForEndGame(victory: true)
    }
    
    func checkForDefeatCondition(defeat: @escaping() -> ()) {
        if colorSumms.red == 0 && colorsSummsRule.red != 0 { defeat(); prepareForEndGame(victory: false) }
        if colorSumms.green == 0 && colorsSummsRule.green != 0 { defeat(); prepareForEndGame(victory: false) }
        if colorSumms.blue == 0 && colorsSummsRule.blue != 0 { defeat(); prepareForEndGame(victory: false) }
        if colorSumms.yellow == 0 && colorsSummsRule.yellow != 0 { defeat(); prepareForEndGame(victory: false) }
        if colorSumms.purple == 0 && colorsSummsRule.purple != 0 { defeat(); prepareForEndGame(victory: false) }
    }
    
    deinit {
        print("GS interactor deallocated")
    }

}

extension GameSceeneInteractor  {
    // Обновлние UI для view
    
    func updateTimer() {
        self.moveElaspedTimeSecondsArray.append(moveElaspedTimeSeconds)
        self.stopTimer()
        moveElaspedTimeSeconds = 0
        output?.updateTimerLabel(value: 0)
        self.startTimer()
    }
    
    func updateLabels() {
        self.updateNumberOfMovesMade()
        self.updatePointsForMove()
    }
    
    func updateNumberOfMovesMade() {
        output?.updateMovesMadeLabel(value: numberOfMovesMade)
    }
    
    func updatePointsForMove() {
        let pointsForMove = -(1 + (moveElaspedTimeSecondsArray.last ?? 0) * 5)
        points += pointsForMove
        output?.updateScoreLabel(value: points)
        switch difficulty {
        case .levelDifficulty: break
        case .hardDifficulty: _dataStorageUpdateHardScore(value: pointsForMove)
        case .hellDifficulty: _dataStorageUpdateHellScore(value: pointsForMove)
        }
    }
}

extension GameSceeneInteractor  {
    // Заполнение массивов и подготовка данных перед началом игры
    
    func fillRandomNumersArray() {
        randomNumbersArray = []
        for _ in 0..<levelDifficulty.numberOfSquares {
            randomNumbersArray.append(Int(arc4random_uniform(UInt32(levelDifficulty.randomNumberUpperBound)))  + 1)
        }
    }
    
    func fillRandomColorsArray() -> Bool {
        
        randomMainColorsArray = []
        randomTintColorsArray = []
        let colors: Array<UIColor> = [.mainYellow, .mainRed, .mainBlue, .mainGreen, .mainPurple]
        for _ in 0..<levelDifficulty.numberOfSquares {
            randomMainColorsArray.append(colors[Int(arc4random_uniform(5))])
        }
        
        for color in colors {
            if !randomMainColorsArray.contains(color) {
                return false
            }
        }
        
        for mainColor in randomMainColorsArray {
            switch mainColor {
            case .mainRed: randomTintColorsArray.append(UIColor.tintRed)
            case .mainGreen: randomTintColorsArray.append(UIColor.tintGreen)
            case .mainBlue: randomTintColorsArray.append(UIColor.tintBlue)
            case .mainYellow: randomTintColorsArray.append(UIColor.tintYellow)
            case .mainPurple: randomTintColorsArray.append(UIColor.tintPurple)
            default: break
            }
        }
        
        return true
    }
    
    func countSummsForColors() {
        colorSumms = ColorsSumms()
        for (index, item) in randomMainColorsArray.enumerated() {
            switch item {
            case .mainYellow: colorSumms.yellow += randomNumbersArray[index]
            case .mainRed: colorSumms.red += randomNumbersArray[index]
            case .mainBlue: colorSumms.blue += randomNumbersArray[index]
            case .mainGreen: colorSumms.green += randomNumbersArray[index]
            case .mainPurple: colorSumms.purple += randomNumbersArray[index]
            default: break
            }
        }
    }
    
    func countSummsRuleForColors() {
        colorsSummsRule = ColorsSummsRule()
        for item in randomNumbersArray {
            let random = arc4random_uniform(5) + 1
            switch random {
            case 1: colorsSummsRule.red += item
            case 2: colorsSummsRule.blue += item
            case 3: colorsSummsRule.green += item
            case 4: colorsSummsRule.purple += item
            case 5: colorsSummsRule.yellow += item
            default: break
            }
        }
    }
}

extension GameSceeneInteractor {
    // Выбор клетки
    
    func setSelectedCellIndexPath(indexPath: IndexPath,
                                  unselectChosenCell: @escaping() -> (),
                                  chooseCorrectCell: @escaping() -> (),
                                  choseIncorrectCell: @escaping() -> (),
                                  relocorCell: @escaping(_ newMainColor: UIColor, _ newTintColor: UIColor) -> (),
                                  recolorCellWithTheSameColor: @escaping() -> (),
                                  victory: @escaping() -> (),
                                  defeat: @escaping() -> ()) {
        
        // если нажать на выбранную слетку или на экран за игровым полем при выбранной клетке (unselect)
        if indexPath == selectedCellIndexPath {
            unselectChosenCell()
            selectedCellIndexPath = nil
            return
        }
        
        // если выбрать клетку (до этого клеток выбранных не было)
        if selectedCellIndexPath == nil {
            chooseCorrectCell()
            selectedCellIndexPath = indexPath
            return
        }
        
        // если выбрать клетку (до этого была выбрана другая клетка) т.е. попробовать перекрасить
        if selectedCellIndexPath != nil {
            tryToChangeColorForIndexPath(indexPath: indexPath, choseIncorrectCell: choseIncorrectCell, recolorCell: relocorCell, recolorCellWithTheSameColor: recolorCellWithTheSameColor)
            checkForWinCondition(victory: victory)
            checkForDefeatCondition(defeat: defeat)
        }
    }
    
    func tryToChangeColorForIndexPath(indexPath: IndexPath,
                                      choseIncorrectCell: @escaping() -> (),
                                      recolorCell: @escaping(_ newMainColor: UIColor, _ newTintColor: UIColor) -> (),
                                      recolorCellWithTheSameColor: @escaping() -> ()) {
        //В предыдущем методе была проверка на нил для selectedCellIndexPath, так что можно разворачивать форсэм
        if indexPath.row <= selectedCellIndexPath!.row + 1 &&
            indexPath.row >= selectedCellIndexPath!.row - 1 &&
            indexPath.section <= selectedCellIndexPath!.section + 1 &&
            indexPath.section >= selectedCellIndexPath!.section - 1 {
            chooseCorrectCellToRecolor(indexPath: indexPath, recolorCell: recolorCell, recolorCellWithTheSameColor: recolorCellWithTheSameColor)
        } else {
            choseIncorrectCell()
            self.selectedCellIndexPath = nil
        }
    }
    
    func chooseCorrectCellToRecolor(indexPath: IndexPath,
                                    recolorCell: @escaping(_ newMainColor: UIColor, _ newTintColor: UIColor) -> (),
                                    recolorCellWithTheSameColor: @escaping() -> ()) {
        let index = selectedCellIndexPath!.row + (selectedCellIndexPath!.section * Int(Double(levelDifficulty.numberOfSquares).squareRoot()))
        
        let mainColorNewValue = randomMainColorsArray[index]
        let tintColorNewValue = randomTintColorsArray[index]
        
        if randomMainColorsArray[indexPath.row + (indexPath.section * Int(Double(levelDifficulty.numberOfSquares).squareRoot()))] == mainColorNewValue {
            //Выбор клетки в зоне досягаемости, но того же цвета. Unselect
            recolorCellWithTheSameColor()
            self.selectedCellIndexPath = nil
            return
        } else {
            randomMainColorsArray[indexPath.row + (indexPath.section * Int(Double(levelDifficulty.numberOfSquares).squareRoot()))] = mainColorNewValue
            randomTintColorsArray[indexPath.row + (indexPath.section * Int(Double(levelDifficulty.numberOfSquares).squareRoot()))] = tintColorNewValue

            
            numberOfMovesMade += 1
            if numberOfMovesMade == 1 { self._dataStorageIncrementGamesPlayed() }
            switch difficulty {
            case .levelDifficulty: break
            case .hardDifficulty: if !_dataStorageHardFirstGamePlayed { _dataStorageHardFirstGamePlayedChangeStatus() }
            case .hellDifficulty: if !_dataStorageHellFirstGamePlayed { _dataStorageHellFirstGamePlayedChangeStatus() }
            }
            
            updateTimer()
            updateLabels()
            
            countSummsForColors()
            output?.updateSummsLabels(colorsSumms: colorSumms, colorsSummsRule: colorsSummsRule)
            
            recolorCell(mainColorNewValue, tintColorNewValue)
            
            self.selectedCellIndexPath = nil
        }
    }
}
