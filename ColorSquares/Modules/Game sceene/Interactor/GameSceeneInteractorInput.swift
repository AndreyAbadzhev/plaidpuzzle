//
//  GameSceeneInteractorInput.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
protocol GameSceeneInteractorInput {
    var difficulty: Difficulty { get }
    var level: Int { get }
    var numberOfSquares: Int? { get }
    var selectedCellIndexPath: IndexPath? { get }
    
    func prepareForStartGame()
    func prepareForEndGame(victory: Bool)
    
    func setSelectedCellIndexPath(indexPath: IndexPath,
                                  unselectChosenCell: @escaping() -> (),
                                  chooseCorrectCell: @escaping() -> (),
                                  choseIncorrectCell: @escaping() -> (),
                                  relocorCell: @escaping(_ newMainColor: UIColor, _ newTintColor: UIColor) -> (),
                                  recolorCellWithTheSameColor: @escaping() -> (),
                                  victory: @escaping() -> (),
                                  defeat: @escaping() -> ())
}
