//
//  GameSceeneInteractorOutput.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
protocol GameSceeneInteractorOutput: class {
    func startGame(difficulty: Difficulty,
                   numbersArray: [Int],
                   mainColorsArray: [UIColor],
                   tintColorsArray: [UIColor],
                   colorsSumms: ColorsSumms,
                   colorsSummsRule: ColorsSummsRule,
                   moves: Int,
                   time: Int,
                   score: Int,
                   isFirstGame: Bool,
                   completion: @escaping() -> ())
    
    func updateMovesMadeLabel(value: Int)
    func updateTimerLabel(value: Int)
    func updateScoreLabel(value: Int)
    func updateSummsLabels(colorsSumms: ColorsSumms, colorsSummsRule: ColorsSummsRule)
}
