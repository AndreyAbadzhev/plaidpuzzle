//
//  GameSceeneViewController.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit
import GoogleMobileAds
class GameSceeneViewController: UIViewController, GameSceeneViewInput, DataStorageProtocol {

    @IBOutlet weak var unselectionView: UIView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var rulesButton: UIButton!

    @IBOutlet weak var levelLabel: UILabel!
    
    //Hard and hell labels
    @IBOutlet weak var hardAndHellLabelsView: UIView!
    @IBOutlet weak var movesMadeLabel: UILabel!
    @IBOutlet weak var moveElaspedTimeLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var separatorView: UIView!
    
    
    //Summ and rule labels
    @IBOutlet weak var summsAndRulesStackView: UIStackView!
    
    @IBOutlet weak var redColorSummLabel: UILabel!
    @IBOutlet weak var greenColorSummLabel: UILabel!
    @IBOutlet weak var blueColorSummLabel: UILabel!
    @IBOutlet weak var yellowColorSummLabel: UILabel!
    @IBOutlet weak var purpleColorSummLabel: UILabel!
    
    @IBOutlet weak var redColorRuleLabel: UILabel!
    @IBOutlet weak var greenColorRuleLabel: UILabel!
    @IBOutlet weak var blueColorRuleLabel: UILabel!
    @IBOutlet weak var yellowColorRuleLabel: UILabel!
    @IBOutlet weak var purpleColorRuleLabel: UILabel!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var output: GameSceeneViewOutput?
    
    var cellsData: [CellData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewIsReady()
    }

    func setupInitialState(completion: @escaping() -> ()) {
        
        //ADS bottom banner
        bannerView.adUnitID = "ca-app-pub-4725748340314469/4475941339" //"ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        
        levelLabel.isHidden = true
        
        self.separatorView.transform = self.separatorView.transform.scaledBy(x: 0.01, y: 1)

        summsAndRulesStackView.alpha = 0
        hardAndHellLabelsView.alpha = 0
        
        collectionView.register(UINib(nibName: String(describing: GameCell.self), bundle: Bundle.main), forCellWithReuseIdentifier: String(describing: GameCell.self))
        
        let unselectTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(unselectCellWhenTapOnScreen))
        unselectionView.addGestureRecognizer(unselectTapRecognizer)
        
        collectionView.reloadData { completion() }
    }
    
    func prepareViewsForReloadGame(completion: @escaping() -> ()) {
        collectionView.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.3, animations: {
            self.separatorView.transform = self.separatorView.transform.scaledBy(x: 0.01, y: 1)
            self.summsAndRulesStackView.alpha = 0
            self.hardAndHellLabelsView.alpha = 0
        }, completion: { _ in
            completion()
        })
    }

    func sendCellsData(cellsDataArray: [CellData]) {
        self.cellsData = cellsDataArray
    }
    
    func startGame(title: String, cellsDataArray: [CellData], additionalFieldHidden: Bool, colorsSumms: ColorsSumms, colorsSummsRule: ColorsSummsRule, moves: String, time: String, score: String, completion: @escaping() -> ()) {
        self.cellsData = cellsDataArray
        
        self.levelLabel.isHidden = false
        self.levelLabel.text = title
        self.movesMadeLabel.isHidden = additionalFieldHidden
        self.moveElaspedTimeLabel.isHidden = additionalFieldHidden
        self.scoreLabel.isHidden = additionalFieldHidden
        
        self.movesMadeLabel.text = moves
        self.moveElaspedTimeLabel.text = time
        self.scoreLabel.text = score
        
        self.redColorSummLabel.text = String(colorsSumms.red); self.redColorRuleLabel.text = String(colorsSummsRule.red)
        self.greenColorSummLabel.text = String(colorsSumms.green); self.greenColorRuleLabel.text = String(colorsSummsRule.green)
        self.blueColorSummLabel.text = String(colorsSumms.blue); self.blueColorRuleLabel.text = String(colorsSummsRule.blue)
        self.yellowColorSummLabel.text = String(colorsSumms.yellow); self.yellowColorRuleLabel.text = String(colorsSummsRule.yellow)
        self.purpleColorSummLabel.text = String(colorsSumms.purple); self.purpleColorRuleLabel.text = String(colorsSummsRule.purple)
        
        self.animateStartGame(completion: { [weak self] in self?.collectionView.isUserInteractionEnabled = true; completion() })
        
    }

    func animateStartGame(completion: @escaping() -> ()) {
        animateCellsAppearence(completion: { [weak self] in self?.animateViewsAppearence(completion: completion) })
    }
    
    func animateCellsAppearence(completion: @escaping() -> ()) {
        var incrementingDelayForCellConstruction = 0.0
        
        for (index, cellData) in cellsData.enumerated() {
            incrementingDelayForCellConstruction += 0.015
            DispatchQueue.main.asyncAfter(deadline: .now() + incrementingDelayForCellConstruction, execute: {
                self.updateCell(indexPath: cellData.indexPath, number: cellData.number, mainColor: cellData.mainColor, tintColor: cellData.tintColor)
                if index == self.cellsData.count - 1 {
                    completion()
                }
            })
        }
    }
    
    func animateViewsAppearence(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, delay: 0.3, animations: {
            self.separatorView.transform = self.separatorView.transform.scaledBy(x: 100, y: 1)
        }, completion: { _ in
            UIView.animate(withDuration: 0.5, animations: {
                self.summsAndRulesStackView.alpha = 1
                self.hardAndHellLabelsView.alpha = 1
            }, completion: { _ in
                completion()
            })
        })
    }
    
    func updateMovesMadeLabel(value: String) {
        movesMadeLabel.text = value
    }
    
    func updateTimerLabel(value: String) {
        moveElaspedTimeLabel.text = value
    }
    
    func updateScoreLabel(value: String) {
        scoreLabel.text = value
    }
    
    func updateCell(indexPath: IndexPath, number: Int?, mainColor: UIColor, tintColor: UIColor) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? GameCell else { return }
        cell.fillCell(number: number, mainColor: mainColor, tintColor: tintColor)
    }
    
    func setCellSelected(indexPath: IndexPath, selectionType: Selection) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? GameCell else { return }
        cell.configureSelect(selection: selectionType, subselectionColor: UIColor.white)
    }
    
    func updateSummsLabels(colorsSumms: ColorsSumms, colorsSummsRule: ColorsSummsRule) {
        
        if redColorSummLabel.text != String(colorsSumms.red) {
            animateUpdatingLabel(label: redColorSummLabel, newValue: String(colorsSumms.red))
        }
        
        if blueColorSummLabel.text != String(colorsSumms.blue) {
            animateUpdatingLabel(label: blueColorSummLabel, newValue: String(colorsSumms.blue))
        }
        
        if greenColorSummLabel.text != String(colorsSumms.green) {
            animateUpdatingLabel(label: greenColorSummLabel, newValue: String(colorsSumms.green))
        }
        
        if purpleColorSummLabel.text != String(colorsSumms.purple) {
            animateUpdatingLabel(label: purpleColorSummLabel, newValue: String(colorsSumms.purple))
        }
        
        if yellowColorSummLabel.text != String(colorsSumms.yellow) {
            animateUpdatingLabel(label: yellowColorSummLabel, newValue: String(colorsSumms.yellow))
        }
    }
    
    func animateUpdatingLabel(label: UILabel, newValue: String) {
        UIView.animate(withDuration: 0.2, animations: {
            label.transform = label.transform.scaledBy(x: 1, y: 0.01)
        }, completion: { finish in
            label.text = newValue
            UIView.animate(withDuration: 0.2, animations: {
                label.transform = label.transform.scaledBy(x: 1, y: 100)
                
            })
        })
    }
    
    @objc func unselectCellWhenTapOnScreen() {
        if let selectedCellIndexPath = output?.selectedCellIndexPath {
            output?.setSelectedCellIndexPath(indexPath: selectedCellIndexPath)
        }
    }
    
    @IBAction func backButtonDidTap() {
        output?.backButtonDidTap()
    }
    
    @IBAction func rulesButtonDidTap() {
        output?.rulesButtonDidTap()
    }
    
    deinit {
        print("GS view deallocated")
    }

}



extension GameSceeneViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Int(Double(output?.numberOfSquares ?? 0).squareRoot())
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(Double(output?.numberOfSquares ?? 0).squareRoot())
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: GameCell.self), for: indexPath) as? GameCell else { return UICollectionViewCell() }
        cell.prepareBlancCell()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let collectionViewsize = Int(view.frame.width - 32)

        let numberOfCellsInRow = Int(Double(output?.numberOfSquares ?? 0).squareRoot())
        let cellSize = collectionViewsize / numberOfCellsInRow
        
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output?.setSelectedCellIndexPath(indexPath: indexPath)
    }
}
