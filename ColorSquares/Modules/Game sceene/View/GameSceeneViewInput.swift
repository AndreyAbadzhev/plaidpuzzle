//
//  GameSceeneViewInput.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
protocol GameSceeneViewInput: class {
    func setupInitialState(completion: @escaping() -> ())
    func prepareViewsForReloadGame(completion: @escaping() -> ())
    func startGame(title: String, cellsDataArray: [CellData], additionalFieldHidden: Bool, colorsSumms: ColorsSumms, colorsSummsRule: ColorsSummsRule, moves: String, time: String, score: String, completion: @escaping() -> ())
    
    func updateTimerLabel(value: String)
    func updateScoreLabel(value: String)
    func updateMovesMadeLabel(value: String)
    func updateSummsLabels(colorsSumms: ColorsSumms, colorsSummsRule: ColorsSummsRule)
    
    func updateCell(indexPath: IndexPath, number: Int?, mainColor: UIColor, tintColor: UIColor)
    func setCellSelected(indexPath: IndexPath, selectionType: Selection)
}
