//
//  GameSceeneViewOutput.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
protocol GameSceeneViewOutput {
    var selectedCellIndexPath: IndexPath? { get }
    var numberOfSquares: Int? { get }
    
    func viewIsReady()
    
    func setSelectedCellIndexPath(indexPath: IndexPath)
    
    func backButtonDidTap()
    func rulesButtonDidTap()
}
