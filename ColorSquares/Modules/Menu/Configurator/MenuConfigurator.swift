//
//  MenuConfigurator.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
class MenuConfigurator {
    
    func configure(with viewController: MenuViewController) {
        let presenter = MenuPresenter()
        let interactor = MenuInteractor()
        let router = MenuRouter(menuViewController: viewController)
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        router.presenter = presenter
    }
    
}
