//
//  MenuInteractor.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import GameKit
class MenuInteractor: MenuInteractorInput, DataStorageProtocol {
    
    var levelNumber: Int {
        return _dataStorageLevel
    }
    
    var gameCenterEnabled = false
    //var gameCenterDefaultLeaderBoard: String?
    
    let hardLeaderboardId = "ColoredCellsHard"
    let hellLeaderboardId = "ColoredCellsHell"

    func authenticateLocalPlayer(completion: @escaping() -> (), showGCLoginViewController: @escaping(UIViewController) -> ()) {
        let localPlayer: GKLocalPlayer = GKLocalPlayer.local
        localPlayer.authenticateHandler = { viewController, error in
            if localPlayer.isAuthenticated {
                self.gameCenterEnabled = true
                completion()
            } else if let GCLoginViewController = viewController {
                showGCLoginViewController(GCLoginViewController) //Контроллер для логина в GC
            } else {
                self.gameCenterEnabled = false
            }
        }
    }
    
    func updateScoreInLeaderboard() {
        if _dataStorageHardFirstGamePlayed {
            let hardScore = GKScore(leaderboardIdentifier: hardLeaderboardId)
            hardScore.value = Int64(_dataStorageHardScore)
            GKScore.report([hardScore]) { (error) in
                if error != nil {
                    print(error!.localizedDescription)
                }
            }
        }
        
        if _dataStorageHellFirstGamePlayed {
            let hellScore = GKScore(leaderboardIdentifier: hellLeaderboardId)
            hellScore.value = Int64(_dataStorageHellScore)
            GKScore.report([hellScore]) { (error) in
                if error != nil {
                    print(error!.localizedDescription)
                }
            }
        }
    }
}
