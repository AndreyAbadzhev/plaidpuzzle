//
//  MenuInteractorInput.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
protocol MenuInteractorInput {
    var levelNumber: Int { get }
    var gameCenterEnabled: Bool { get }
    //var leaderboardId: String { get }
    func authenticateLocalPlayer(completion: @escaping() -> (), showGCLoginViewController: @escaping(UIViewController) -> ())
    func updateScoreInLeaderboard()
}
