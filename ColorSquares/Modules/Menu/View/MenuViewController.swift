//
//  MenuViewController.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit
import AVFoundation

class MenuViewController: UIViewController, MenuViewInput {
    
    @IBOutlet weak var gameNameHeaderView: UIView!
    @IBOutlet weak var headerLabel: UILabel!

    @IBOutlet weak var levelsButton: UIButton!
    @IBOutlet weak var hardcoreButton: UIButton!
    @IBOutlet weak var hellButton: UIButton!
    @IBOutlet weak var leaderboardButton: UIButton!

    @IBOutlet weak var lineHeadingToSettingsButtonView: UIView!
    @IBOutlet weak var settingsButton: UIButton!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var output: MenuViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameNameHeaderView.frame = CGRect(x: 0, y: 0, width: gameNameHeaderView.frame.width, height: 0)
        
        lineHeadingToSettingsButtonView.frame = CGRect(x: 30, y: lineHeadingToSettingsButtonView.frame.minY, width: 1, height: 1)
        settingsButton.frame = CGRect(x: settingsButton.frame.minX, y: view.frame.height + 200, width: settingsButton.frame.width, height: settingsButton.frame.height)
        
        levelsButton.transform = levelsButton.transform.scaledBy(x: 1, y: 0.01)
        hardcoreButton.transform = hardcoreButton.transform.scaledBy(x: 1, y: 0.01)
        hellButton.transform = hellButton.transform.scaledBy(x: 1, y: 0.01)
        leaderboardButton.transform = leaderboardButton.transform.scaledBy(x: 0.01, y: 0.01)
        
        headerLabel.textColor = UIColor.gameBackgroundColor
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output?.viewIsReady()
    }

    func setUpInitialState() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.playHeaderAnimation), name: NSNotification.Name(rawValue: "playHeaderAnimation"), object: nil)
        
        levelsButton.setTitle("Play \(output?.levelNumber ?? 1) level".uppercased(), for: .normal)
        hardcoreButton.setTitle("Play Hard".uppercased(), for: .normal)
        hellButton.setTitle("Play Hell".uppercased(), for: .normal)

        hardcoreButton.setTitleColor(UIColor.mainRed, for: .normal)
        hellButton.setTitleColor(UIColor.black, for: .normal)

        animateViewAppear()
    }
    
    func animateViewAppear() {
        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "playHeaderAnimation")))

        UIView.animate(withDuration: 0.3, delay: 0, animations: {
            self.gameNameHeaderView.frame = CGRect(x: 0, y: 0, width: self.gameNameHeaderView.frame.width, height: 200)
        }, completion: { _ in
            self.headerLabel.textColor = UIColor.gameTitleColor
        })
        
        UIView.animate(withDuration: 0.2, delay: 0.1, animations: {
            self.levelsButton.transform = self.levelsButton.transform.scaledBy(x: 1, y: 100)
        })
        
        UIView.animate(withDuration: 0.2, delay: 0.2, animations: {
            self.hardcoreButton.transform = self.hardcoreButton.transform.scaledBy(x: 1, y: 100)
        })
        
        UIView.animate(withDuration: 0.2, delay: 0.3, animations: {
            self.hellButton.transform = self.hellButton.transform.scaledBy(x: 1, y: 100)
        })
        
        UIView.animate(withDuration: 0.2, delay: 0.4, animations: {
            self.leaderboardButton.transform = self.leaderboardButton.transform.scaledBy(x: 100, y: 100)
        })
        
        UIView.animate(withDuration: 0.2, delay: 0.5, animations: {
            self.settingsButton.frame = CGRect(x: self.settingsButton.frame.minX, y: self.view.frame.height - 20 - self.settingsButton.frame.height, width: self.settingsButton.frame.width, height: self.settingsButton.frame.height)
        }, completion: { _ in
            UIView.animate(withDuration: 0.1, delay: 0, animations: {
            self.lineHeadingToSettingsButtonView.frame = CGRect(x: 30, y: self.lineHeadingToSettingsButtonView.frame.minY, width: self.view.frame.width - 90 - self.settingsButton.frame.width, height: 1)
            })
        })
    }
    
    func animateViewDisappear(completion: @escaping() -> ()) {
        self.headerLabel.textColor = UIColor.gameBackgroundColor
        
        UIView.animate(withDuration: 0.3, animations: {
            self.gameNameHeaderView.frame = CGRect(x: 0, y: 0, width: self.gameNameHeaderView.frame.width, height: 0)

            self.lineHeadingToSettingsButtonView.frame = CGRect(x: 30, y: self.lineHeadingToSettingsButtonView.frame.minY, width: 1, height: 1)
            self.settingsButton.frame = CGRect(x: self.settingsButton.frame.minX, y: self.view.frame.height + 200, width: self.settingsButton.frame.width, height: self.settingsButton.frame.height)

            self.levelsButton.transform = self.levelsButton.transform.scaledBy(x: 1, y: 0.01)
            self.hardcoreButton.transform = self.hardcoreButton.transform.scaledBy(x: 1, y: 0.01)
            self.hellButton.transform = self.hellButton.transform.scaledBy(x: 1, y: 0.01)
            self.leaderboardButton.transform = self.leaderboardButton.transform.scaledBy(x: 0.01, y: 0.01)
            
            NotificationCenter.default.removeObserver(self)
        }, completion: { _ in
            completion()
        })
    }
    
    @objc func playHeaderAnimation() {
        UIView.animate(withDuration: 1.2, delay: 0, animations: {
            self.headerLabel.alpha = 0.1
        }, completion: { _ in
            UIView.animate(withDuration: 1.2, delay: 0, animations: {
                self.headerLabel.alpha = 1
            }, completion: { _ in
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "playHeaderAnimation")))
            })
        })
    }

    @IBAction func startGameButtonDidTap(_ sender: UIButton) {
        output?.playSoundTap()
        animateViewDisappear(completion: {
            switch sender {
            case self.levelsButton: self.output?.startGameButtonDidTap(difficulty: .levelDifficulty)
            case self.hardcoreButton: self.output?.startGameButtonDidTap(difficulty: .hardDifficulty)
            case self.hellButton: self.output?.startGameButtonDidTap(difficulty: .hellDifficulty)
            default: break
            }
        })
    }

    @IBAction func leaderboardButtonDidTap() {
        output?.playSoundTap()
        if output?.gameCenterEnabled == true {
            animateViewDisappear(completion: { self.output?.leaderboardButtonDidTap() })
        } else {
            output?.authenticateLocalPlayer(showGCLoginViewController: { [weak self] loginViewController in
                self?.output?.showGameCenterLoginViewController(viewController: loginViewController)
            }) // Не работает, хз почему
            
            output?.gameCenterUnavailable() // работает
        }
    }
    
    @IBAction func settingsButtonDidTapped() {
        output?.playSoundTap()
        animateViewDisappear(completion: { self.output?.settingsButtonDidTapped() })
    }
}
