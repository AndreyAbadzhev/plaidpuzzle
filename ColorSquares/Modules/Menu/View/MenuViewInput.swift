//
//  MenuViewInput.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
protocol MenuViewInput: class {
    func setUpInitialState()
}
