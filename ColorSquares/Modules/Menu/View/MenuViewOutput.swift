//
//  MenuViewOutput.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
protocol MenuViewOutput {
    var levelNumber: Int? { get }
    var gameCenterEnabled: Bool? { get }
    func viewIsReady()
    func startGameButtonDidTap(difficulty: Difficulty)
    func leaderboardButtonDidTap()
    func authenticateLocalPlayer(showGCLoginViewController: @escaping(UIViewController) -> ())
    func showGameCenterLoginViewController(viewController: UIViewController)
    func settingsButtonDidTapped()
    func playSoundTap()
    func gameCenterUnavailable()
}
