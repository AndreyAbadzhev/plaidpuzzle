//
//  MenuPresenter.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
class MenuPresenter: MenuViewOutput, SoundsProtocol {
    weak var view: MenuViewInput?
    var interactor: MenuInteractorInput?
    var router: MenuRouterInput?
    
    var levelNumber: Int? {
        return interactor?.levelNumber
    }
    
    var gameCenterEnabled: Bool? {
        return interactor?.gameCenterEnabled
    }
    
    func viewIsReady() {
        interactor?.updateScoreInLeaderboard()
        
        interactor?.authenticateLocalPlayer(completion: { [weak self] in
            self?.interactor?.updateScoreInLeaderboard()
        }, showGCLoginViewController: { _ in } )
        view?.setUpInitialState()
    }
    
    func startGameButtonDidTap(difficulty: Difficulty) {
        router?.openGameSceene(difficulty: difficulty)
    }
    
    func settingsButtonDidTapped() {
        router?.openSettings()
    }
    
    func playSoundTap() {
        _soundsPlaySound(soundType: .tap)
    }
    
    func leaderboardButtonDidTap() {
        router?.openGameCenter(leaderboardIdentifier: nil, completion: { [weak self] in self?.view?.setUpInitialState() })
    }
    
    func authenticateLocalPlayer(showGCLoginViewController: @escaping (UIViewController) -> ()) {
        interactor?.authenticateLocalPlayer(completion: { [weak self] in
            self?.interactor?.updateScoreInLeaderboard()
        }, showGCLoginViewController: showGCLoginViewController)
    }
    
    func showGameCenterLoginViewController(viewController: UIViewController) {
        router?.showGameCenterLoginViewController(viewController: viewController)
    }
    
    func gameCenterUnavailable() {
        router?.showAlertGameCenterUnavailable()
    }
}

extension MenuPresenter: MenuRouterOutput {
    func gameCenterViewControllerDissmissed() {
        view?.setUpInitialState()
    }
}
