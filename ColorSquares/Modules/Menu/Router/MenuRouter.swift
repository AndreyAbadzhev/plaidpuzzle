//
//  MenuRouter.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import GameKit
class MenuRouter: NSObject, MenuRouterInput {

    weak var view: MenuViewController?
    weak var presenter: MenuRouterOutput?
    
    required init(menuViewController: MenuViewController) {
        self.view = menuViewController
    }
    
    func openGameSceene(difficulty: Difficulty) {
        let gameSceeneViewController = GameSceeneViewController()
        let gameSceeneConfigurator = GameSceeneConfigurator(difficulty: difficulty)
        gameSceeneConfigurator.configure(with: gameSceeneViewController)
        view?.present(gameSceeneViewController, animated: false, completion: nil)
    }
    
    func openGameCenter(leaderboardIdentifier: String?, completion: @escaping () -> ()) {
        //guard let leaderboardId = leaderboardIdentifier else { return }
        let gameCenterViewController = GKGameCenterViewController()
        gameCenterViewController.gameCenterDelegate = self
        gameCenterViewController.viewState = .leaderboards
        //gameCenterViewController.leaderboardIdentifier = leaderboardId
        view?.present(gameCenterViewController, animated: true, completion: nil)
    }
    
    func showGameCenterLoginViewController(viewController: UIViewController) {
        view?.present(viewController, animated: true, completion: nil)
    }
    
    func openSettings() {
        let settingsViewController = SettingsViewController()
        view?.present(settingsViewController, animated: false, completion: nil)
    }
    
    func showAlertGameCenterUnavailable() {
        let alertView = Bundle.main.loadNibNamed(String(describing: AlertView.self), owner: nil, options: nil)?[0] as? AlertView
        alertView?.titleLabel.text = "Ooops..."
        alertView?.messageLabel.text = "Something went wrong. It could be some problems withyour internet connection, or you just not logged in Game Center."
        alertView?.leftButton.setTitle("I got it", for: .normal)
        alertView?.leftButton.setTitleColor(UIColor.tintGreen, for: .normal)
        alertView?.leftButtonAction = { [weak alertView] in alertView?.hide { alertView?.removeFromSuperview() } }
        alertView?.rightButton.isHidden = true
        alertView?.frame = view?.view.frame ?? CGRect()
        view?.view.addSubview(alertView ?? AlertView())
        alertView?.show()
    }
}

extension MenuRouter: GKGameCenterControllerDelegate {
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: { [weak self] in self?.presenter?.gameCenterViewControllerDissmissed() } )
    }
}
