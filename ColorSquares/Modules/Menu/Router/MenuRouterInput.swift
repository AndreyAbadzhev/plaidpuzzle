//
//  MenuRouterInput.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
protocol MenuRouterInput {
    func openGameSceene(difficulty: Difficulty)
    func openGameCenter(leaderboardIdentifier: String?, completion: @escaping() -> ())
    func showGameCenterLoginViewController(viewController: UIViewController)
    func openSettings()
    func showAlertGameCenterUnavailable()
}
