//
//  MenuRouterOutput.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 07/05/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
protocol MenuRouterOutput: class {
    func gameCenterViewControllerDissmissed()
}
