//
//  SettingsViewController.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 25/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, DataStorageProtocol, SoundsProtocol {

    @IBOutlet weak var soundsSwitch: UISwitch!
    @IBOutlet weak var musicSwitch: UISwitch!
    
    @IBOutlet weak var gamesPlayedValueLabel: UILabel!
    
    @IBOutlet weak var hardScoreLabel: UILabel!
    @IBOutlet weak var hellScroreLabel: UILabel!

    @IBOutlet weak var clearProgressDebugButton: UIButton!
    @IBOutlet weak var addProgressDebugButton: UIButton!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        soundsSwitch.isOn = !_dataStorageSoundsMuted
        musicSwitch.isOn = !_dataStorageMusicMuted
        
        gamesPlayedValueLabel.text = String(_dataStorageGamesPlayed)
        
        hardScoreLabel.text = String(_dataStorageHardScore)
        hellScroreLabel.text = String(_dataStorageHellScore)
        
        clearProgressDebugButton.isHidden = true
        addProgressDebugButton.isHidden = true
    }

    
    @IBAction func soundsSwitchChangedValue() {
        _dataStorageSoundsMutedChangeStatus()
        _soundsPlaySound(soundType: .tap)
    }
    
    @IBAction func musicSwitchChangedValue() {
        _dataStorageMusicMutedChangeStatus()
        _dataStorageMusicMuted ? Music.shared().pause() : Music.shared().play()
        _soundsPlaySound(soundType: .tap)
    }
    
    @IBAction func clearProgressDebugButtonDidTap() {
        _dataStorageDebugClearProgress()
    }
    
    @IBAction func addProgressDebugButtonDidTap() {
        _dataStorageIncrementLevel()
    }
    
    @IBAction func creditsButtonDidTap() {
        _soundsPlaySound(soundType: .tap)
        let alertView = Bundle.main.loadNibNamed(String(describing: AlertView.self), owner: nil, options: nil)?[0] as? AlertView
        alertView?.titleLabel.text = "CREDITS"
        alertView?.messageLabel.text = "Programming and design\nAndrey Abadzhev\n-\nMoral support\nEvgenia Semennikova\n-\nMusic\nwww.bensound.com"
        alertView?.leftButton.setTitle("Sweet", for: .normal)
        alertView?.leftButton.setTitleColor(UIColor.tintGreen, for: .normal)
        alertView?.leftButtonAction = { [weak alertView] in alertView?.hide { alertView?.removeFromSuperview() } }
        alertView?.rightButton.isHidden = true
        alertView?.frame = view.frame
        view.addSubview(alertView ?? AlertView())
        alertView?.show()
    }
    
    @IBAction func closeButtonTapped() {
        _soundsPlaySound(soundType: .tap)
        self.dismiss(animated: false, completion: nil)
    }
    
    deinit {
        print("SettingsVC deallocated")
    }
}
