//
//  OnboardingAlertView.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 08/05/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit

class OnboardingAlertView: UIView, SoundsProtocol {

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pageNumberLabel: UILabel!
    
    @IBOutlet weak var okButton: UIButton!
    var okButtonAction: (() -> ())?
    
    var descriptionsArray = ["Select a square you like!",
                             "Then tap a neibour square! For example this yellow one!",
                             "Look! This yellow became red too!",
                             "Also the summ of red squares changed! It is 8. But need 18!",
                             "Now make this numbers equal for all the colors!"]

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: String(describing: OnboardingCell.self), bundle: .main), forCellWithReuseIdentifier: String(describing: OnboardingCell.self))
        titleLabel.text = "RULES"
        okButton.setTitle("I got it", for: .normal)
        
        self.alpha = 0
        contentView.layer.cornerRadius = 3
        
        separatorView.transform = separatorView.transform.scaledBy(x: 0.01, y: 1)
        
        collectionView.alpha = 0
        descriptionLabel.text = ""
        pageNumberLabel.text = ""
        okButton.alpha = 0
        
        okButton.setTitleColor(UIColor.textColorDiabled, for: .normal)
        okButton.isUserInteractionEnabled = false
    }
    
    func show() {
        showSelf { self.showContent() }
    }
    
    func showSelf(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        }, completion: { _ in
            completion()
        })
    }
    
    func showContent() {
        UIView.animate(withDuration: 0.3, animations: {
            self.separatorView.transform = self.separatorView.transform.scaledBy(x: 100, y: 1)
            
            self.collectionView.alpha = 1
            self.okButton.alpha = 1
        })
        self.scrollViewDidEndDecelerating(self.collectionView)
    }
    
    func hide(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }, completion: { _ in
            completion()
        })
    }
    
    deinit {
        print("Onboarding alert view deallocated")
    }
    
    
    @IBAction func okButtonDidTap() {
        _soundsPlaySound(soundType: .tap)
        okButtonAction?()
    }
    
}

extension OnboardingAlertView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: OnboardingCell.self), for: indexPath) as? OnboardingCell else { return UICollectionViewCell() }
        cell.fillCell(index: indexPath.row + 1)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.2, animations: {
            self.descriptionLabel.alpha = 0
        })
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        _soundsPlaySound(soundType: .cellUnselect)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexOfCell = (Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))
        
        self.pageNumberLabel.text = "\(indexOfCell + 1)/\(self.collectionView.numberOfItems(inSection: 0))"
        self.descriptionLabel.text = self.descriptionsArray[indexOfCell]
            
        UIView.animate(withDuration: 0.2, animations: {
            self.descriptionLabel.alpha = 1
            
            if indexOfCell == self.collectionView.numberOfItems(inSection: 0) - 1 {
                self.okButton.setTitleColor(UIColor.tintGreen, for: .normal)
                self.okButton.isUserInteractionEnabled = true
            }
        })
    }
}
