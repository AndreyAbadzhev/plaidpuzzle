//
//  OnboardingCell.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 08/05/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit

class OnboardingCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    func fillCell(index: Int) {
        switch index {
        case 1: imageView.image = UIImage(named: "onboardingPageShortFirst")
        case 2: imageView.image = UIImage(named: "onboardingPageShortSecond")
        case 3: imageView.image = UIImage(named: "onboardingPageShortThird")
        case 4: imageView.image = UIImage(named: "onboardingPageShortFourth")
        case 5: imageView.image = UIImage(named: "onboardingPageShortFifth")
        default: break
        }
    }

}
