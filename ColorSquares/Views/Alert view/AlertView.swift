//
//  AlertView.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 27/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit

class AlertView: UIView, SoundsProtocol {

    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    var leftButtonAction: (() -> ())?
    var rightButtonAction: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.alpha = 0
        contentView.layer.cornerRadius = 3
        
        separatorView.transform = separatorView.transform.scaledBy(x: 0.01, y: 1)
        messageLabel.alpha = 0
        leftButton.alpha = 0
        rightButton.alpha = 0
    }
    
    @IBAction func leftButtonDidTap() {
        _soundsPlaySound(soundType: .tap)
        leftButtonAction?()
    }
    
    @IBAction func rightButtonDidTap() {
        _soundsPlaySound(soundType: .tap)
        rightButtonAction?()
    }
    
    func show() {
        showSelf { self.showContent() }
    }
    
    func showSelf(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        }, completion: { _ in
            completion()
        })
    }

    func showContent() {
        UIView.animate(withDuration: 0.3, animations: {
            self.separatorView.transform = self.separatorView.transform.scaledBy(x: 100, y: 1)
            
            self.messageLabel.alpha = 1
            self.leftButton.alpha = 1
            self.rightButton.alpha = 1
        })
    }
    
    func hide(completion: @escaping() -> ()) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }, completion: { _ in
            completion()
        })
    }
    
    deinit {
        print("Alert view deallocated")
    }
}
