//
//  Cell.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 09/05/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
struct CellData {
    let indexPath: IndexPath
    let number: Int?
    let mainColor: UIColor
    let tintColor: UIColor
    
    init(indexPath: IndexPath, number: Int, mainColor: UIColor, tintColor: UIColor) {
        self.indexPath = indexPath
        self.number = number
        self.mainColor = mainColor
        self.tintColor = tintColor
    }
}
