//
//  Difficulty.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
enum Difficulty {
    case levelDifficulty
    case hardDifficulty
    case hellDifficulty
}

struct LevelDifficulty {
    let randomNumberUpperBound: Int
    let numberOfSquares: Int
    let level: Int
    
    init(difficulty: Difficulty, level: Int) {
        self.level = level
        switch difficulty {
        case .levelDifficulty: randomNumberUpperBound = level + 4; numberOfSquares = 25
        case .hardDifficulty: randomNumberUpperBound = 14; numberOfSquares = 25
        case .hellDifficulty: randomNumberUpperBound = 39;numberOfSquares = 36
        }
    }
}
