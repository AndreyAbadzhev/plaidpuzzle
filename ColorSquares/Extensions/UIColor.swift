//
//  UIColor.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 23/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    static var mainRed: UIColor {
        return UIColor(red: 245/255, green: 119/255, blue: 80/255, alpha: 1.0)
    }
    
    static var mainGreen: UIColor {
        return UIColor(red: 139/255, green: 197/255, blue: 66/255, alpha: 1.0)
    }
    
    static var mainBlue: UIColor {
        return UIColor(red: 4/255, green: 168/255, blue: 248/255, alpha: 1.0)
    }
    
    static var mainYellow: UIColor {
        return UIColor(red: 255/255, green: 225/255, blue: 25/255, alpha: 1.0)
    }
    
    static var mainPurple: UIColor {
        return UIColor(red: 158/255, green: 100/255, blue: 179/255, alpha: 1.0)
    }
    
    static var tintRed: UIColor {
        return UIColor(red: 200/255, green: 65/255, blue: 43/255, alpha: 1.0)
    }
    
    static var tintGreen: UIColor {
        return UIColor(red: 71/255, green: 135/255, blue: 75/255, alpha: 1.0)
    }
    
    static var tintBlue: UIColor {
        return UIColor(red: 17/255, green: 100/255, blue: 248/255, alpha: 1.0)
    }
    
    static var tintYellow: UIColor {
        return UIColor(red: 232/255, green: 145/255, blue: 0/255, alpha: 1.0)
    }
    
    static var tintPurple: UIColor {
        return UIColor(red: 103/255, green: 15/255, blue: 186/255, alpha: 1.0)
    }
    
    static var textColorNormal: UIColor {
        return UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1.0)
    }
    
    static var textColorDiabled: UIColor {
        return UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1.0)
    }
    
    static var gameTitleColor: UIColor {
        return UIColor(red: 255/255, green: 243/255, blue: 195/255, alpha: 1.0)
    }
    
    static var gameBackgroundColor: UIColor {
        return UIColor(red: 255/255, green: 241/255, blue: 231/255, alpha: 1.0)
    }
}
