//
//  UICollectionView.swift
//  ColorSquares
//
//  Created by Roman Sukhar on 27/04/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import Foundation
import UIKit
extension UICollectionView {
    func reloadData(completion: @escaping () -> ()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion: { _ in
            completion()
        })
    }
}
