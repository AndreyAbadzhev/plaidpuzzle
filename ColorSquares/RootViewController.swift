//
//  ViewController.swift
//  ColorSquares
//
//  Created by Andrey on 21/03/2019.
//  Copyright © 2019 Andrey. All rights reserved.
//

import UIKit

class RootViewController: UIViewController, DataStorageProtocol {
    override func viewDidAppear(_ animated: Bool) {
        _dataStorageTryMakeInitialRecord()
        
        super.viewDidAppear(animated)
        let menuViewController = MenuViewController()
        let menuConfigurator = MenuConfigurator()
        menuConfigurator.configure(with: menuViewController)
        self.present(menuViewController, animated: false, completion: nil)
    }
}

